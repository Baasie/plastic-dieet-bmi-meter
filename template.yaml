AWSTemplateFormatVersion: '2010-09-09'
Transform: 'AWS::Serverless-2016-10-31'

Description: Insert description here

Globals:
  Function:
    Runtime: nodejs8.10
    Timeout: 60
  Api:
    Cors:
      AllowMethods: "'GET,POST,OPTIONS'"
      AllowHeaders: "'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range'"
      AllowOrigin: !Sub "'https://${CloudFront.DomainName}'"

Parameters:
  Stage:
    Type: String
    Default: local
    Description: Parameterize the stage of the deployment

Resources:
  PlasticDieetApp:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub "plasticdieetapp-${Stage}"
      AccessControl: PublicRead
      WebsiteConfiguration:
        IndexDocument: index.html
  CloudFront:
    Type: AWS::CloudFront::Distribution
    Properties:
      DistributionConfig:
        Origins:
        - DomainName: !Sub "${PlasticDieetApp}.s3-website-eu-west-1.amazonaws.com"
          Id: "origin"
          CustomOriginConfig:
            OriginProtocolPolicy: "http-only"
        Enabled: True
        CustomErrorResponses:
        - ErrorCode: 404
          ResponseCode: 200
          ResponsePagePath: "/index.html"
        DefaultCacheBehavior:
          AllowedMethods:
          - DELETE
          - GET
          - HEAD
          - OPTIONS
          - PATCH
          - POST
          - PUT
          TargetOriginId: "origin"
          DefaultTTL: 1
          ForwardedValues:
            QueryString: True
            Cookies:
              Forward: none
          ViewerProtocolPolicy: allow-all
        PriceClass: PriceClass_100
  RegisterPlasticDayCommandHandler:
    Type: 'AWS::Serverless::Function'
    Properties:
      MemorySize: 128
      CodeUri: dist/backend/
      FunctionName: !Sub "pdm-calculatebmi-${Stage}"
      Handler: calculate-bmi.handle
      Description: FaaS handler for the calculate-bmi-lamda APIGatewayEvent
      ReservedConcurrentExecutions: 20
      Environment:
        Variables:
          PLASTIC_DAY_EVENTS_TABLE_NAME: !Ref 'PlasticDietBMIEventStore'
          PLASTIC_DIEET_CORS_DOMAIN: !Sub "https://${CloudFront.DomainName}"
      Events:
        PostRegisterPlasticDay:
          Type: Api
          Properties:
            Path: /plastic-dieet/{personId}
            Method: post
      Policies:
        - Version: "2012-10-17"
          Statement:
            - Effect: "Allow"
              Action:
                - "dynamoDb:PutItem"
                - "dynamoDb:Query"
              Resource:
                !Sub 'arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/PlasticDietBMIEventStore-${Stage}*'
  FindAllCalculatedBMIEventsQuery:
    Type: 'AWS::Serverless::Function'
    Properties:
      MemorySize: 128
      CodeUri: dist/backend/
      FunctionName: !Sub "pdb-findallcalculatedbmievents-${Stage}"
      Handler: find-all-calculated-bmi.handle
      ReservedConcurrentExecutions: 20
      Description: FaaS handler for the get-all-calcualted-bmi APIGatewayEvent
      Environment:
        Variables:
          PLASTIC_DAY_EVENTS_TABLE_NAME: !Ref 'PlasticDietBMIEventStore'
          PLASTIC_DIEET_CORS_DOMAIN: !Sub "https://${CloudFront.DomainName}"
      Events:
        PostRegisterPlasticDay:
          Type: Api
          Properties:
            Path: /plastic-dieet/{personId}
            Method: get
      Policies:
      - Version: "2012-10-17"
        Statement:
        - Effect: "Allow"
          Action:
          - "dynamoDb:Query"
          Resource:
            !Sub 'arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/PlasticDietBMIEventStore-${Stage}*'
  PlasticDietBMIEventStore:
    Type: 'AWS::DynamoDB::Table'
    Properties:
      TableName: !Sub "PlasticDietBMIEventStore-${Stage}"
      AttributeDefinitions:
        - AttributeName: personId
          AttributeType: S
        - AttributeName: day
          AttributeType: S
      KeySchema:
        - AttributeName: personId
          KeyType: HASH
        - AttributeName: day
          KeyType: RANGE
      LocalSecondaryIndexes:
        - IndexName: personDay
          KeySchema:
            - AttributeName: personId
              KeyType: HASH
            - AttributeName: day
              KeyType: RANGE
          Projection:
            ProjectionType: KEYS_ONLY
      ProvisionedThroughput:
        ReadCapacityUnits: 5
        WriteCapacityUnits: 5

Outputs:
  PlasticDieetApi:
    Description: "API Gateway endpoint URL for Prod stage for Hello World function"
    Value: !Sub "https://${ServerlessRestApi}.execute-api.${AWS::Region}.amazonaws.com/Prod/plastic-dieet/"
  PlasticDieetUrl:
    Description: "The url of the API Gateway"
    Value: !Sub "https://${CloudFront.DomainName}"
