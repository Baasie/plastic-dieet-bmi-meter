module.exports = function (config) {
    config.set({
        files: [
            {
                pattern: 'src/**/*.ts',
                mutated: true,
                included: false
            },
            {
                pattern: 'tests/**/*.ts',
                mutated: false,
                included: true
            }
        ],
        testRunner: "jest",
        mutator: "typescript",
        transpilers: ["typescript"],
        reporter: ["clear-text", "progress", "html"],
        coverageAnalysis: "off",
        tsconfigFile: "tsconfig.json",
        mutate: ["src/**/*.ts"]
    });
};
