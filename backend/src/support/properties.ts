export async function resolvePlasticDayEventsTableName(): Promise<string> {
    if (!process.env.PLASTIC_DAY_EVENTS_TABLE_NAME) {
        return Promise.reject('env property PLASTIC_DAY_EVENTS_TABLE_NAME not found.');
    }
    return process.env.PLASTIC_DAY_EVENTS_TABLE_NAME.toString();
}

export function resolvePlasticDieetCORSDomain(): string {
    if (!process.env.PLASTIC_DIEET_CORS_DOMAIN) {
       throw new Error('env property PLASTIC_DIEET_CORS_DOMAIN not found.');
    }
    return process.env.PLASTIC_DIEET_CORS_DOMAIN.toString();
}

