import {APIGatewayEvent, Callback, Context} from "aws-lambda";
import {Response} from "../support/response";
import {PersonId} from "../domain/plastic-categories";
import {getBMICalculatedEventsByPersonId} from "../port.adapter.dynamodb/document-client";

export function handle(event: APIGatewayEvent, context: Context, callback: Callback) {
    let personId: PersonId;
    try {
        personId = validatePersonId(event);
    } catch (error) {
        return callback(null, Response.BAD_REQUEST(error));
    }

    getBMICalculatedEventsByPersonId(personId).then(
        events => callback(null, Response.OK(JSON.stringify(events.toJSON()))),
        error => callback(null, Response.INTERNAL_SERVER_ERROR(error)),
    );
}

function validatePersonId(event: APIGatewayEvent): PersonId {
    if (event.pathParameters) {
        return new PersonId(event.pathParameters.personId);
    } else {
        throw new Error('invalid personId');
    }
}
