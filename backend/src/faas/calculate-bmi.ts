import {APIGatewayEvent, Callback, Context} from 'aws-lambda';
import {CalculateBMICommand} from '../domain/bmi-meter-commands';
import {BMICalculatedEvent} from '../domain/bmi-meter-events';
import {handleCalculateBMICommand} from '../domain/bmi-calculator';
import {doesJournalEventExist, putBMICalculatedEvent} from '../port.adapter.dynamodb/document-client';
import {Response} from '../support/response';
import {PersonId} from "../domain/plastic-categories";

export function handle(event: APIGatewayEvent, context: Context, callback: Callback) {
    let calculateBMICommand: CalculateBMICommand;
    let personId: PersonId;
    try {
        calculateBMICommand = CalculateBMICommand.fromJSON(JSON.parse(event.body as string));
        personId = validatePersonId(event);
    } catch (error) {
        return callback(null, Response.BAD_REQUEST(error));
    }

    doesJournalEventExist(personId, calculateBMICommand.day).then(doesExist => {
        if (doesExist) {
            return callback(null, Response.CONFLICT('BMI Already Exists for that day.'));
        } else {
            let bmiCalculatedEvent: BMICalculatedEvent = handleCalculateBMICommand(personId, calculateBMICommand);

            putBMICalculatedEvent(bmiCalculatedEvent).then(data => {
                return callback(null, Response.OK(JSON.stringify(data.toJSON())));
            }, error => {
                return callback(null, Response.INTERNAL_SERVER_ERROR(error));
            });
        }
    }, error => {
        return callback(null, Response.INTERNAL_SERVER_ERROR(error));
    });
}

function validatePersonId(event: APIGatewayEvent): PersonId {
    if(event.pathParameters){
        return new PersonId(event.pathParameters.personId);
    } else {
        throw new Error('invalid personId');
    }
}


