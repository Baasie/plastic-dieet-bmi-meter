import {TinyType, TinyTypeOf} from 'tiny-types';
import {DocumentClient, PutItemInput, PutItemInputAttributeMap} from 'aws-sdk/clients/dynamodb';
import {DynamoDB} from 'aws-sdk';
import * as moment from 'moment';
import {Moment} from 'moment';
import {resolvePlasticDayEventsTableName} from '../support/properties';
import {BMICalculatedEvent, BMICalculatedEvents} from '../domain/bmi-meter-events';
import {PersonId} from "../domain/plastic-categories";

const documentClient: DocumentClient = new DynamoDB.DocumentClient();

const LOCAL_DATE_FORMAT = 'YYYY_MM_DD';

// noinspection TypeScriptValidateTypes
export class Persistence_Id extends TinyTypeOf<string>() {
}

export class LocalDate extends TinyType {
    static fromJSON = (date: string) => new LocalDate(moment(date, LOCAL_DATE_FORMAT));

    constructor(public readonly localDate: Moment) {
        super();
    }
}

export function doesJournalEventExist(personId: PersonId, day: LocalDate): Promise<boolean> {
    return resolvePlasticDayEventsTableName().then(tableName => {
            const params = {
                TableName: tableName,
                KeyConditionExpression: '#personId = :personId and #day = :day',
                ExpressionAttributeNames: {
                    '#personId': 'personId',
                    '#day': 'day'
                },
                ExpressionAttributeValues: {
                    ':personId': personId.personId,
                    ':day': day.localDate.toISOString()
                },
                Select: 'COUNT'
            };

            return documentClient.query(params).promise().then(
                data => data.Count !== undefined && data.Count > 0,
                error => {
                    return Promise.reject(error.message)
                },
            );
        },
    );
}

export function putBMICalculatedEvent(bmiCalculatedEvent: BMICalculatedEvent): Promise<BMICalculatedEvent> {
    return resolvePlasticDayEventsTableName().then(tableName => {
        const params: PutItemInput = {
            TableName: tableName,
            Item: bmiCalculatedEvent.toJSON() as PutItemInputAttributeMap,
        };
        return documentClient.put(params).promise().then(
            data => Promise.resolve(bmiCalculatedEvent),
            error => Promise.reject(error)
        );
    });
}

export function getBMICalculatedEventsByPersonId(personId: PersonId): Promise<BMICalculatedEvents> {
    return resolvePlasticDayEventsTableName().then(tableName => {
            const params = {
                TableName: tableName,
                KeyConditionExpression: '#personId = :personId',
                ExpressionAttributeNames: {
                    '#personId': 'personId',
                },
                ExpressionAttributeValues: {
                    ':personId': personId.personId,
                }
            };

            return documentClient.query(params).promise().then(
                data => Promise.resolve(parseBMICalculatedEvent(data.Items)),
                error => {
                    return Promise.reject(error.message)
                },
            );
        },
    );
}

function parseBMICalculatedEvent(items): BMICalculatedEvents {
    let bmiCalculatedEvents: Array<BMICalculatedEvent> = [];
    items.forEach(item => {
        bmiCalculatedEvents.push(BMICalculatedEvent.fromJSON(item as any));
    });
    return new BMICalculatedEvents(bmiCalculatedEvents);
}