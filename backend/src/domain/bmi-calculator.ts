import {BMICalculatedEvent} from './bmi-meter-events';
import {CalculateBMICommand} from './bmi-meter-commands';
import {BMI, PersonId, PlasticCategories} from './plastic-categories';

const STRAWS_VALUE: number = 2;
const BAGS_VALUE: number = 2;
const BOTTLES_VALUE: number = 2;
const CUPS_VALUE: number = 2;
const SMALL_PACKS_VALUE: number = 1;
const BIG_PACKS_VALUE: number = 1.5;
const PARTLY_PLASTIC_PACKS_VALUE: number = 1;
const CUTLERYS_VALUE: number = 1;
const PLATES_VALUE: number = 1;

const BMI_FACTOR: number = 7;

export function handleCalculateBMICommand(personId: PersonId, calculateBMICommand: CalculateBMICommand): BMICalculatedEvent {
    return new BMICalculatedEvent(personId, calculateBMICommand.day, calculateBMICommand.plasticCategories, calculateBMI(calculateBMICommand.plasticCategories));
}

function calculateBMI(plasticCategories: PlasticCategories): BMI {

    const sumOfDayEvent: number = plasticCategories.numberOfStraws.amount * STRAWS_VALUE
        + plasticCategories.numberOfBags.amount * BAGS_VALUE
        + plasticCategories.numberOfBottles.amount * BOTTLES_VALUE
        + plasticCategories.numberOfCups.amount * CUPS_VALUE
        + plasticCategories.numberOfSmallPacks.amount * SMALL_PACKS_VALUE
        + plasticCategories.numberOfBigPacks.amount * BIG_PACKS_VALUE
        + plasticCategories.numberOfPartlyPlasticPacks.amount * PARTLY_PLASTIC_PACKS_VALUE
        + plasticCategories.numberOfCutlerys.amount * CUTLERYS_VALUE
        + plasticCategories.numberOfPlates.amount * PLATES_VALUE;


    return new BMI(sumOfDayEvent / BMI_FACTOR);
}