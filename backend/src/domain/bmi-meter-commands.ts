import {check, ensure, isDefined, Serialised, TinyType} from 'tiny-types';
import {LocalDate, PersonId, PlasticCategories} from './plastic-categories';
import {isNullOrUndefined} from "util";

export class CalculateBMICommand extends TinyType {
    static fromJSON = (o: Serialised<CalculateBMICommand>) => new CalculateBMICommand(
        LocalDate.fromJSON(o.day as string),
        PlasticCategories.fromJSON(o.plasticCategories as any),
    );

    constructor(public readonly day: LocalDate, public readonly plasticCategories: PlasticCategories) {
        super();
        ensure('Day', day, isDefined());
        ensure('Plastic Categories', plasticCategories, isDefined())
    }
}
