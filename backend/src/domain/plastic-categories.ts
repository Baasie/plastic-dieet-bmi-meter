import {ensure, isDefined, isInRange, isInteger, isString, Predicate, Serialised, TinyType} from 'tiny-types';
import * as moment from 'moment';
import {Moment} from 'moment';

const LOCAL_DATE_FORMAT = 'YYYY-MM-DD';


export class PlasticCategories extends TinyType {
    static fromJSON = (o: Serialised<PlasticCategories>) =>
        new PlasticCategories(
            Amount.fromJSON(o.numberOfBags as number),
            Amount.fromJSON(o.numberOfStraws as number),
            Amount.fromJSON(o.numberOfBottles as number),
            Amount.fromJSON(o.numberOfCups as number),
            Amount.fromJSON(o.numberOfSmallPacks as number),
            Amount.fromJSON(o.numberOfBigPacks as number),
            Amount.fromJSON(o.numberOfPartlyPlasticPacks as number),
            Amount.fromJSON(o.numberOfCutlerys as number),
            Amount.fromJSON(o.numberOfPlates as number)
        );


    constructor(public readonly numberOfBags: Amount, public readonly numberOfStraws: Amount,
                public readonly numberOfBottles: Amount, public readonly numberOfCups: Amount,
                public readonly numberOfSmallPacks: Amount, public readonly numberOfBigPacks: Amount,
                public readonly numberOfPartlyPlasticPacks: Amount, public readonly numberOfCutlerys: Amount,
                public readonly numberOfPlates: Amount) {
        super();
        ensure('Number of Bags', numberOfBags, isDefined());
        ensure('Number of Straws', numberOfStraws, isDefined());
        ensure('Number of Bottles', numberOfBottles, isDefined());
        ensure('Number of Cups', numberOfCups, isDefined());
        ensure('Number of Small Packs', numberOfSmallPacks, isDefined());
        ensure('Number of Big Packs', numberOfBigPacks, isDefined());
        ensure('Number of Partly Plastic Packs', numberOfPartlyPlasticPacks, isDefined());
        ensure('Number of Cutlerys', numberOfCutlerys, isDefined());
        ensure('Number of Plates', numberOfPlates, isDefined());

    }
}

// noinspection TypeScriptValidateTypes
export class PersonId extends TinyType {
    static fromJSON = (id: string) => new PersonId(id);

    constructor(public readonly personId: string) {
        super();
        ensure("PersonId", personId, isDefined(), isString());
    }
}

export class Amount extends TinyType {
    static fromJSON = (amount: number) => new Amount(amount);

    constructor(public readonly amount: number) {
        super();
        ensure("Amount", amount, isDefined(), isInteger(), isInRange(0, 1000))
    }
}

export class LocalDate extends TinyType {
    static fromJSON = (date: string) => new LocalDate(moment(date, LOCAL_DATE_FORMAT, true));

    toJSON(): string {
        return this.localDate.format(LOCAL_DATE_FORMAT);
    }

    constructor(public readonly localDate: Moment) {
        super();
        ensure('Day', localDate, isDefined(), isValidDate());
    }
}

function isValidDate(): Predicate<Moment> {
    return Predicate.to(`be a valid date`, (value: Moment) => value.isValid());
}

// noinspection TypeScriptValidateTypes
export class BMI extends TinyType {
    static fromJSON = (bmi: number) => new BMI(bmi);

    constructor(public readonly bmi: number) {
        super();
    }
}