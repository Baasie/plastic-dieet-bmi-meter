import {ensure, isDefined, Serialised, TinyType} from 'tiny-types';
import {BMI, LocalDate, PersonId, PlasticCategories} from './plastic-categories';


export class BMICalculatedEvents extends TinyType {

    constructor(public readonly bmiCalculatedEvents: Array<BMICalculatedEvent>) {
        super();
        ensure('BMICalculatedEvents', bmiCalculatedEvents, isDefined());
    }
}

export class BMICalculatedEvent extends TinyType {
    public static fromJSON = (o: Serialised<BMICalculatedEvent>) => new BMICalculatedEvent(
        PersonId.fromJSON(o.personId as string),
        LocalDate.fromJSON(o.day as string),
        PlasticCategories.fromJSON(o.plasticCategories as any),
        BMI.fromJSON(o.bmi as number),
    );

    constructor(public readonly personId: PersonId, public readonly day: LocalDate, public readonly plasticCategories: PlasticCategories, public readonly bmi: BMI) {
        super();
        ensure('PersonId', personId, isDefined());
        ensure('Day', day, isDefined());
        ensure('Plastic Categories', plasticCategories, isDefined());
        ensure('BmiDay', bmi, isDefined());
    }
}