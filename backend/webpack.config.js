// Import path for resolving file paths
const path = require('path');
const fs = require("fs");

module.exports = {
    target: 'node', // in order to ignore built-in modules like path, fs, etc.
    mode: 'production',
    externals: {
        'aws-sdk': 'aws-sdk',
    },

    // Specify the entry point for our lambda's.
    entry: fs.readdirSync(path.join(__dirname, "./src/faas"))
        .filter(filename => /\.ts$/.test(filename))
        .map(filename => {
            let entry = {};
            entry[filename.replace(".ts", "")] = path.join(
                __dirname,
                "./src/faas",
                filename
            );
            console.log(entry);
            return entry;
        })
        .reduce((finalObject, entry) => Object.assign(finalObject, entry), {}),

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },

    // Specify the output file containing our bundled code
    output: {
        path: path.join(__dirname, "./../dist/backend"),
        library: "[name]",
        libraryTarget: "commonjs2",
        filename: "[name].js"
    },
    module: {
        rules: [
            {
                test: /\.json$/,
                exclude: /node_modules/,
                loaders: ['json']
            },
            {
                test: /\.tsx?$/,
                exclude: /tests/,
                loader: 'ts-loader',
                options: {
                    configFile: 'tsconfig.json'
                }
            }
        ]
    }
};