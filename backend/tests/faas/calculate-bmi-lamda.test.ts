import {handle} from '../../src/faas/calculate-bmi';
import {APIGatewayEvent} from 'aws-lambda';
import {
    BASIC_CALCULATEDBMICOMMAND_APIGATEWAYEVENT,
    EMPTY_CONTEXT,
    FAULTY_BODY_CALCULATEDBMICOMMAND_APIGATEWAYEVENT,
    FAULTY_PATHPARAM_CALCULATEDBMICOMMAND_APIGATEWAYEVENT
} from './apigatewayproxyevent.fixtures';
import {doesJournalEventExist, putBMICalculatedEvent} from '../../src/port.adapter.dynamodb/document-client';
import {BASIC_CALCULATEBMIEVENT} from "../domain/bmi-meter-commands.fixtures";

jest.mock('../../src/port.adapter.dynamodb/document-client');

const mockResolveTableName = jest.fn(() => 'kb-local');
jest.mock('../../src/support/properties', () => ({
    resolvePlasticDieetCORSDomain: () => mockResolveTableName(),
}));

describe('Calculate BmiDay Lambda', () => {
    beforeEach(() => {
        doesJournalEventExist.mockResolvedValue(Promise.resolve(false));
        putBMICalculatedEvent.mockResolvedValue(Promise.resolve(BASIC_CALCULATEBMIEVENT));
    });

    it('should throw BAD_REQUEST on wrong event body', done => {
        handle(FAULTY_BODY_CALCULATEDBMICOMMAND_APIGATEWAYEVENT as APIGatewayEvent, EMPTY_CONTEXT, (err, data) => {
            expect(err).toBeNull();
            expect(data).toBeDefined();
            expect(data.statusCode).toBe(400);
            done();
        });
    });

    it('should throw BAD_REQUEST on wrong event pathParameter', done => {
        handle(FAULTY_PATHPARAM_CALCULATEDBMICOMMAND_APIGATEWAYEVENT as APIGatewayEvent, EMPTY_CONTEXT, (err, data) => {
            expect(err).toBeNull();
            expect(data).toBeDefined();
            expect(data.statusCode).toBe(400);
            done();
        });
    });

    it('should throw CONFLICT when event already exsists', done => {
        doesJournalEventExist.mockResolvedValue(Promise.resolve(true));
        handle(BASIC_CALCULATEDBMICOMMAND_APIGATEWAYEVENT as APIGatewayEvent, EMPTY_CONTEXT, (err, data) => {
            expect(err).toBeNull();
            expect(data).toBeDefined();
            expect(data.statusCode).toBe(409);
            done();
        });
    });

    it('should throw INTERNAL_SERVER_ERROR when doesJournalEventExist fails', done => {
        doesJournalEventExist.mockResolvedValue(Promise.reject());
        handle(BASIC_CALCULATEDBMICOMMAND_APIGATEWAYEVENT as APIGatewayEvent, EMPTY_CONTEXT, (err, data) => {
            expect(err).toBeNull();
            expect(data).toBeDefined();
            expect(data.statusCode).toBe(500);
            done();
        });
    });

    it('should throw INTERNAL_SERVER_ERROR when putBMICalculatedEvent fails', done => {
        putBMICalculatedEvent.mockResolvedValue(Promise.reject());
        handle(BASIC_CALCULATEDBMICOMMAND_APIGATEWAYEVENT as APIGatewayEvent, EMPTY_CONTEXT, (err, data) => {
            expect(err).toBeNull();
            expect(data).toBeDefined();
            expect(data.statusCode).toBe(500);
            done();
        });
    });

    it('should return OK when putBMICalculatedEvent succeeds', done => {
        handle(BASIC_CALCULATEDBMICOMMAND_APIGATEWAYEVENT as APIGatewayEvent, EMPTY_CONTEXT, (err, data) => {
            expect(err).toBeNull();
            expect(data).toBeDefined();
            expect(data.statusCode).toBe(200);
            done();
        });
    });
});