export const EMPTY_CONTEXT: any = {};

export const BASIC_CALCULATEDBMICOMMAND_APIGATEWAYEVENT = {
    pathParameters: {
        personId: '1337',
    },
    body: '{\n' +
        '  "day": "2018-02-02",\n' +
        '  "plasticCategories": {\n' +
        '    "numberOfBags": 2,\n' +
        '    "numberOfStraws": 5,\n' +
        '    "numberOfBottles": 6,\n' +
        '    "numberOfCups": 7,\n' +
        '    "numberOfSmallPacks": 9,\n' +
        '    "numberOfBigPacks": 20,\n' +
        '    "numberOfPartlyPlasticPacks": 1,\n' +
        '    "numberOfCutlerys": 4,\n' +
        '    "numberOfPlates": 7\n' +
        '  }\n' +
        '}',
};

export const FAULTY_PATHPARAM_CALCULATEDBMICOMMAND_APIGATEWAYEVENT = {
    body: '{\n' +
        '  "day": "2018-02-02",\n' +
        '  "plasticCategories": {\n' +
        '    "numberOfBags": 2,\n' +
        '    "numberOfStraws": 5,\n' +
        '    "numberOfBottles": 6,\n' +
        '    "numberOfCups": 7,\n' +
        '    "numberOfSmallPacks": 9,\n' +
        '    "numberOfBigPacks": 20,\n' +
        '    "numberOfPartlyPlasticPacks": 1,\n' +
        '    "numberOfCutlerys": 4,\n' +
        '    "numberOfPlates": 7\n' +
        '  }\n' +
        '}',
};

export const FAULTY_BODY_CALCULATEDBMICOMMAND_APIGATEWAYEVENT = {
    pathParameters: {
        personId: '1337',
    },
    body: '{\n' +
        '  "plasticCategories": {\n' +
        '    "numberOfBags": 2,\n' +
        '    "numberOfStraws": 5,\n' +
        '    "numberOfBottles": 6,\n' +
        '    "numberOfCups": 7,\n' +
        '    "numberOfSmallPacks": 9,\n' +
        '    "numberOfBigPacks": 20,\n' +
        '    "numberOfPartlyPlasticPacks": 1,\n' +
        '    "numberOfCutlerys": 4,\n' +
        '    "numberOfPlates": 7\n' +
        '  }\n' +
        '}',
};

export const BASIC_GETALLCALCULATEDBMI_APIGATEWAYEVENT = {
    pathParameters: {
        personId: '1337',
    },
    body: '',
};