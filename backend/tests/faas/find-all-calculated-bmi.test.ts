import {handle} from "../../src/faas/find-all-calculated-bmi";
import {BASIC_GETALLCALCULATEDBMI_APIGATEWAYEVENT, EMPTY_CONTEXT} from "./apigatewayproxyevent.fixtures";
import {getBMICalculatedEventsByPersonId} from "../../src/port.adapter.dynamodb/document-client";
import {BASIC_BMICALCULATEDEVENTS} from "../domain/bmi-meter-events.fixtures";

jest.mock('../../src/port.adapter.dynamodb/document-client');

const mockResolveTableName = jest.fn(() => 'kb-local');
jest.mock('../../src/support/properties', () => ({
    resolvePlasticDieetCORSDomain: () => mockResolveTableName(),
}));

describe('Get all calculated BmiDay Lambda', () => {
    beforeEach(() => {
        getBMICalculatedEventsByPersonId.mockResolvedValue(Promise.resolve(BASIC_BMICALCULATEDEVENTS));
    });

    it('should return all calculated BmiDay', done => {
        handle(BASIC_GETALLCALCULATEDBMI_APIGATEWAYEVENT, EMPTY_CONTEXT, (err, data) => {
            console.log(data);
            expect(err).toBeNull();
            expect(data).toBeDefined();
            expect(data.statusCode).toBe(200);
            let bmiCalculatedEvents = JSON.parse(data.body);
            expect(bmiCalculatedEvents).toBeDefined();
            done();
        })
    });

});
