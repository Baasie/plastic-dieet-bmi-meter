import {BMICalculatedEvent, BMICalculatedEvents} from '../../src/domain/bmi-meter-events';
import {BMI, LocalDate, PersonId} from '../../src/domain/plastic-categories';
import {BASIC_PLASTICCATEGORIES} from './plastic-categories.fixtures';

export const BASIC_BMICALCULATEDEVENT: BMICalculatedEvent = new BMICalculatedEvent(new PersonId('1337'), LocalDate.fromJSON('2018-02-02'), BASIC_PLASTICCATEGORIES, new BMI(10));

export const BASIC_BMICALCULATEDEVENT_ARRAY: Array<BMICalculatedEvent> = [BASIC_BMICALCULATEDEVENT, BASIC_BMICALCULATEDEVENT];

export const BASIC_BMICALCULATEDEVENTS: BMICalculatedEvents = new BMICalculatedEvents(BASIC_BMICALCULATEDEVENT_ARRAY);