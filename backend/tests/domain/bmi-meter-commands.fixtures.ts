import {CalculateBMICommand} from '../../src/domain/bmi-meter-commands';
import {LocalDate} from '../../src/domain/plastic-categories';
import {BASIC_PLASTICCATEGORIES} from './plastic-categories.fixtures';

export const BASIC_CALCULATEBMIEVENT = new CalculateBMICommand(LocalDate.fromJSON('2018-02-02'), BASIC_PLASTICCATEGORIES);