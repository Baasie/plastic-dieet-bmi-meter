import {handleCalculateBMICommand} from '../../src/domain/bmi-calculator';
import {BASIC_CALCULATEBMIEVENT} from './bmi-meter-commands.fixtures';
import {BMICalculatedEvent} from '../../src/domain/bmi-meter-events';
import {PersonId} from "../../src/domain/plastic-categories";

//TODO JSVERIFY?!?!
describe('handleCalculateBMICommand', () => {
    it('should handleCalculateBMICommand', () => {
        const bmiCalculatedEvent: BMICalculatedEvent = handleCalculateBMICommand(new PersonId('1337'), BASIC_CALCULATEBMIEVENT);
        expect(bmiCalculatedEvent).toBeDefined();
    });
});
