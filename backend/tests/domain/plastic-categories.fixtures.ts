import {Amount, PlasticCategories} from '../../src/domain/plastic-categories';

export const BASIC_PLASTICCATEGORIES: PlasticCategories = new PlasticCategories(new Amount(1), new Amount(3), new Amount(5), new Amount(3),
    new Amount(4), new Amount(5), new Amount(10), new Amount(1), new Amount(0));