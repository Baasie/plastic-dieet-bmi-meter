import {doesJournalEventExist, LocalDate, putBMICalculatedEvent} from '../../src/port.adapter.dynamodb/document-client';
import {BASIC_BMICALCULATEDEVENT} from "../domain/bmi-meter-events.fixtures";
import {PersonId} from "../../src/domain/plastic-categories";

const mockResolveTableName = jest.fn(() => Promise.resolve('table'));
jest.mock('../../src/support/properties', () => ({
    resolvePlasticDayEventsTableName: () => mockResolveTableName(),
}));

let mockCount = 1;
const mockQueryResult = jest.fn(() => Promise.resolve({
    Count: mockCount
}));
const mockPutResult = jest.fn(() => Promise.resolve({}));
jest.mock('aws-sdk', () => ({
    DynamoDB: {
        DocumentClient: class {
            query() {
                return {
                    promise: mockQueryResult
                }
            };

            put() {
                return {
                    promise: mockPutResult
                }
            };
        }
    }
}));

describe('Document Client', function () {

    describe('doesJournalEventExist', () => {

        beforeEach(() => {
            mockCount = 1;
        });

        it('should return true when Count is 1', async () => {
            const outcome = await doesJournalEventExist(PersonId.fromJSON('1337'), LocalDate.fromJSON('2018-08-08'));
            expect(outcome).toBe(true);
        });

        it('should return true when Count is more than 1', async () => {
            mockCount = 2;
            const outcome = await doesJournalEventExist(PersonId.fromJSON('1337'), LocalDate.fromJSON('2018-08-08'));
            expect(outcome).toBe(true);
        });

        it('should return false when Count is 0', async () => {
            mockCount = 0;
            const outcome = await doesJournalEventExist(PersonId.fromJSON('1337'), LocalDate.fromJSON('2018-08-08'));
            expect(outcome).toBe(false);
        });

        it('should return error when resolvePlasticDayEventsTableName fails', () => {
            mockResolveTableName.mockRejectedValueOnce('error');
            const outcome = doesJournalEventExist(PersonId.fromJSON('1337'), LocalDate.fromJSON('2018-08-08'));
            return expect(outcome).rejects.toEqual('error');
        });

        it('should return a rejected promise', () => {
            mockQueryResult.mockRejectedValueOnce({message: 'error'});
            const outcome = doesJournalEventExist(PersonId.fromJSON('1337'), LocalDate.fromJSON('2018-08-08'));
            return expect(outcome).rejects.toEqual('error');
        });
    });

    describe('putBMICalculatedEvent', () => {

        it('should return object when put succeeds', async () => {
            const outcome = await putBMICalculatedEvent(BASIC_BMICALCULATEDEVENT);
            expect(outcome).toEqual(BASIC_BMICALCULATEDEVENT);
        });

        it('should return a rejected promise when put fails', () => {
            mockPutResult.mockRejectedValue({message: 'error'});
            const outcome = putBMICalculatedEvent(BASIC_BMICALCULATEDEVENT);
            return expect(outcome).rejects.toEqual({message: 'error'});
        });

        it('should return error when resolvePlasticDayEventsTableName fails', () => {
            mockResolveTableName.mockRejectedValueOnce('error');
            const outcome = putBMICalculatedEvent(BASIC_BMICALCULATEDEVENT);
            return expect(outcome).rejects.toEqual('error');
        });
    });

    //TODO implement me!
    describe('getBMICalculatedEventsByPersonId', () => {
        it('should return BMICalculatedEvents', () => {

        })
    })
});


