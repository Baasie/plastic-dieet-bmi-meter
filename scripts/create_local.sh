#!/usr/bin/env bash
set -e -u

export AWS_REGION="eu-west-1"
export aws_stage="kb-local"

cd backend
npm run test
npm run build

cd ../app
npm run build
cd ..

aws cloudformation package --region eu-west-1 --template-file template.yaml --s3-bucket baasie.plastic-dieet.release --s3-prefix ${aws_stage} --output-template-file packaged.yaml
aws cloudformation deploy --region eu-west-1 --template-file ./packaged.yaml --stack-name plastic-dieet-${aws_stage} --parameter-overrides Stage=${aws_stage} --capabilities CAPABILITY_IAM

json -I -f ./dist/app/environment.json -e "this.API_URL='$(aws cloudformation describe-stacks --stack-name plastic-dieet-$aws_stage --query 'Stacks[0].Outputs[?OutputKey==`PlasticDieetApi`].OutputValue' --output text)'"
json -I -f ./dist/app/environment.json -e "this.DOMAIN_URL='$(aws cloudformation describe-stacks --stack-name plastic-dieet-$aws_stage --query 'Stacks[0].Outputs[?OutputKey==`PlasticDieetUrl`].OutputValue' --output text)'"

aws s3 rm s3://plasticdieetapp-${aws_stage} --recursive
aws s3 cp ./dist/app/ s3://plasticdieetapp-${aws_stage}  --recursive --acl public-read

pwd
sed  -i -e "s^export API_URL=.*^export API_URL=$(aws cloudformation describe-stacks --stack-name plastic-dieet-$aws_stage --query 'Stacks[0].Outputs[?OutputKey==`PlasticDieetApi`].OutputValue' --output text)^" ./scripts/local_variables.sh
sed  -i -e "s^export DOMAIN_URL=.*^export DOMAIN_URL=$(aws cloudformation describe-stacks --stack-name plastic-dieet-$aws_stage --query 'Stacks[0].Outputs[?OutputKey==`PlasticDieetUrl`].OutputValue' --output text)^" ./scripts/local_variables.sh