#!/usr/bin/env bash
set -e -u

export AWS_REGION="eu-west-1"
export aws_stage="kb-local"

cd ./app
npm run build
cd ..

json -I -f ./dist/app/environment.json -e "this.API_URL='$(aws cloudformation describe-stacks --stack-name plastic-dieet-$aws_stage --query 'Stacks[0].Outputs[?OutputKey==`PlasticDieetApi`].OutputValue' --output text)'"
json -I -f ./dist/app/environment.json -e "this.DOMAIN_URL='$(aws cloudformation describe-stacks --stack-name plastic-dieet-$aws_stage --query 'Stacks[0].Outputs[?OutputKey==`PlasticDieetUrl`].OutputValue' --output text)'"

aws s3 rm s3://plasticdieetapp-${aws_stage} --recursive
aws s3 cp ./dist/app/ s3://plasticdieetapp-${aws_stage}  --recursive --acl public-read
