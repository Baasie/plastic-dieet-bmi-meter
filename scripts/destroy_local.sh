#!/usr/bin/env bash
set -e -u

aws s3 rm s3://plasticdieetapp-kb-local --recursive

aws cloudformation delete-stack --region eu-west-1 --stack-name plastic-dieet-kb-local