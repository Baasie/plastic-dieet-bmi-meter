#!/usr/bin/env bash
set -e -u

SCRIPT_DIR=$PWD/ci

mkdir -p dist
mkdir -p backend
mkdir -p backend/dist
aws s3 cp s3://${RELEASE_BUCKET}/release-candidates/${RELEASE_NAME}.zip ./dist/${RELEASE_NAME}.zip
unzip -o ./dist/${RELEASE_NAME}.zip -d ./dist
cp -r ./dist/backend ./backend/dist

echo "Destroying infra on ${AWS_REGION}"
aws cloudformation delete-stack --region ${AWS_REGION} --stack-name plastic-dieet-${aws_stage}