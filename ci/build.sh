#!/usr/bin/env bash

echo "npm Version:"
npm -v

echo "npm install for backend"
cd backend
npm i --silent

echo "run tests backend"
npm run test

echo "run build backend"
npm run build

echo "npm install for app"
cd ../app
npm i
npm run build
cd ..

echo "Zipping dist to ${RELEASE_NAME}.zip"
mkdir -p release

cp template.yaml dist/template.yaml
cd dist
zip -r ./../release/release.zip .
cd ..

echo "Uploading zip to releases artifactory"
aws s3 cp ./release/release.zip s3://${RELEASE_BUCKET}/release-candidates/${RELEASE_NAME}.zip