#!/usr/bin/env bash

echo "npm Version:"
npm -v

echo "npm install"
npm i --silent

echo "Mutation testing"
npm run test:mutation