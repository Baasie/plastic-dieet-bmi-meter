#!/usr/bin/env bash
set -e -u

SCRIPT_DIR=$PWD/ci

mkdir -p dist
aws s3 cp s3://${RELEASE_BUCKET}/release-candidates/${RELEASE_NAME}.zip ./dist/${RELEASE_NAME}.zip
unzip -o ./dist/${RELEASE_NAME}.zip -d ./dist

echo "Deploying infra on ${AWS_REGION} with stage ${aws_stage}"

aws cloudformation package --region ${AWS_REGION} --template-file template.yaml --s3-bucket baasie.cd-faas.release --s3-prefix ${aws_stage} --output-template-file packaged.yaml
aws cloudformation deploy --region ${AWS_REGION} --template-file ./packaged.yaml --stack-name plastic-dieet-${aws_stage} --parameter-overrides Stage=${aws_stage} --capabilities CAPABILITY_IAM

aws s3 rm s3://plasticdieetapp-${aws_stage} --recursive
export API_URL=$(aws cloudformation --region $AWS_REGION describe-stacks --stack-name plastic-dieet-$aws_stage --query 'Stacks[0].Outputs[?OutputKey==`PlasticDieetApi`].OutputValue' --output text)
export DOMAIN_URL=$(aws cloudformation --region $AWS_REGION describe-stacks --stack-name plastic-dieet-$aws_stage --query 'Stacks[0].Outputs[?OutputKey==`PlasticDieetUrl`].OutputValue' --output text)

echo "API_URL ${API_URL}"
echo "API_URL ${DOMAIN_URL}"
json -I -f ./dist/app/environment.json -e "this.API_URL='$API_URL'"
json -I -f ./dist/app/environment.json -e "this.DOMAIN_URL='$DOMAIN_URL'"

aws s3 cp ./dist/app/ s3://plasticdieetapp-${aws_stage} --recursive --acl public-read

