import {EnvironmentState} from '@/state/environment/types';
import {UserState} from '@/state/user/types';

export interface RootState {
    environment: EnvironmentState;
    user: UserState;
}
