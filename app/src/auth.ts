import auth0, {WebAuth} from 'auth0-js';
import _Vue from 'vue';
import {Store} from 'vuex';
import store from './store';

export function AuthPlugin(Vue: typeof _Vue, options?: any): void {
    Vue.prototype.$auth = new Auth(store);
}

export class Auth {
    private store: Store<any>;
    private domainUrl: string = '';

    constructor(store: Store<any>) {
        this.store = store;
    }

    private webAuth(): WebAuth {
        return new auth0.WebAuth({
            domain: 'opgemarkt.eu.auth0.com',
            clientID: 'EzoBh0W5zTpcJNdq6UjXHh84uhSTzlbb',
            redirectUri: this.store.state.environment.environment.DOMAIN_URL + '/callback',
            audience: 'https://opgemarkt.eu.auth0.com/api/v2/',
            responseType: 'token id_token',
            scope: 'openid profile',
        });
    }

    public getToken(): any {
        return localStorage.getItem('id_token');
    }

    public setToken(id_token: any): any {
        localStorage.setItem('id_token', id_token);
    }

    public getAccessToken(): any {
        return localStorage.getItem('access_token');
    }

    public setAccessToken(accessToken: any): any {
        localStorage.setItem('access_token', accessToken);
    }

    public getExpiresAt(): any {
        return localStorage.getItem('expires_at') || '{}';
    }

    public setExpiresAt(expiresIn: any): any {
        const expiresAt = JSON.stringify(expiresIn * 1000 + new Date().getTime());
        localStorage.setItem('expires_at', expiresAt);
    }

    public getUser(): any {
        return JSON.parse(localStorage.getItem('user') || '{}');
    }

    public setUser(user: any): any {
        localStorage.setItem('user', JSON.stringify(user));

    }


    public login() {
        this.webAuth().authorize();
    }

    public logout() {
        return new Promise((resolve, reject) => {
            localStorage.removeItem('access_token');
            localStorage.removeItem('id_token');
            localStorage.removeItem('expires_at');
            localStorage.removeItem('user');
            this.webAuth().authorize();
        });
    }

    public isAuthenticated(): boolean {
        return new Date().getTime() < this.getExpiresAt();
    }

    public loadUserInfo() {
        this.webAuth().client.userInfo(this.getAccessToken(), (err, profile) => {
            this.store.commit('user/userLoaded', profile);
        });
    }

    public handleAuthentication() {
        return new Promise((resolve, reject) => {
            this.webAuth().parseHash((err, authResult) => {
                if (authResult && authResult.accessToken && authResult.idToken) {
                    this.setExpiresAt(authResult.expiresIn);
                    this.setAccessToken(authResult.accessToken);
                    this.setToken(authResult.idToken);
                    this.setUser(authResult.idTokenPayload);
                    this.loadUserInfo();
                    resolve();
                } else if (err) {
                    this.logout();
                    reject(err);
                }

            });
        });
    }
}

declare module 'vue/types/vue' {
    interface Vue {
        $auth: Auth;
    }
}
