import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import BmiMeter from './views/BmiMeter.vue';
import BmiCalendar from './components/BmiCalendar.vue';
import BmiDetails from './components/BmiDetails.vue';
import BmiInput from './components/BmiInput.vue';
import BmiCalculated from './components/BmiCalculated.vue';
import Callback from './views/Callback.vue';
import store from "@/store";

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/habit-tracker',
            name: 'habit-tracker',
            component: BmiMeter,
            children: [
                {
                    path: 'calendar',
                    name: 'bmi-calendar',
                    component: BmiCalendar,
                },
                {
                    path: 'details/:bmiDay',
                    name: 'bmi-details',
                    component: BmiDetails,
                    props: true,
                },
                {
                    path: 'input/:day',
                    name: 'bmi-input',
                    component: BmiInput,
                    props: true,
                },
                {
                    path: 'calculated',
                    name: 'bmi-calculated',
                    component: BmiCalculated,
                    props: true,
                },
            ],
        },
        {
            path: '/callback',
            name: 'callback',
            component: Callback,
        },
        {
            path: '/why',
            name: 'why',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/Why.vue'),
        },
        {
            path: '/map',
            name: 'map',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/Map.vue'),
        },
        {
            path: '/what',
            name: 'what',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/What.vue'),
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
        },
    ],
});

router.beforeEach((to, from, next) => {
    if(!store.state.user.user && router.app.$auth.getAccessToken()) {
        router.app.$auth.loadUserInfo();
    }
    if (to.name === 'callback') { // check if "to"-route is "callback" and allow access
        next();
    } else if (router.app.$auth.isAuthenticated()) { // if authenticated allow access
        next();
    } else { // trigger auth0 login
        router.app.$auth.login();
    }
});

export default router;
