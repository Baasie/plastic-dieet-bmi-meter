import Vue from 'vue';
import App from './App.vue';
import router from './router';
import {AuthPlugin} from './auth';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import {ApiPlugin} from '@/api';
import store from '@/store';

Vue.use(BootstrapVue);
Vue.use(ApiPlugin);
Vue.use(AuthPlugin);
Vue.config.productionTip = false;

store.dispatch('environment/fetchData').then(
    data => {
        new Vue({
            router,
            store,
            render: (h) => h(App),
        }).$mount('#app');
    }
);

