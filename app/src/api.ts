import axios, {AxiosInstance, AxiosResponse} from 'axios';
import {BMICalculatedEvent, BMICalculatedEvents} from '@/domain/bmi-meter-events';
import {CalculateBMICommand} from '@/domain/bmi-meter-commands';
import _Vue from 'vue';
import store from './store';
import {Store} from 'vuex';

export function ApiPlugin(Vue: typeof _Vue, options?: any): void {
    Vue.prototype.$http = new Api(store);
}

export default class Api {
    private store: Store<any>;

    constructor(store: Store<any>) {
        this.store = store;
    }

    private async client(): Promise<AxiosInstance> {
        const baseURL = this.store.state.environment.environment.API_URL;
        return axios.create({
            baseURL,
            timeout: 4000,
            headers: {Accept: 'application/json,application/xml'},
        });
    }

    public async findBMICalculatedEventsByUser(): Promise<BMICalculatedEvents> {
        const userId = this.store.state.user.user.sub;
        const client: AxiosInstance = await this.client();
        const response: AxiosResponse<any> = await client.get(userId);
        return BMICalculatedEvents.fromJSON(response.data);
    }

    public async calculateBmiForUser(calculateBMICommand: CalculateBMICommand): Promise<number> {
        const userId = this.store.state.user.user.sub;
        const client: AxiosInstance = await this.client();
        const response: AxiosResponse<any> = await client.post(userId, calculateBMICommand.toJSON());
        const bmiCalculatedEvent: BMICalculatedEvent = BMICalculatedEvent.fromJSON(response.data);
        return bmiCalculatedEvent.bmi.bmi;
    }
}

declare module 'vue/types/vue' {
    interface Vue {
        $http: Api;
    }
}
