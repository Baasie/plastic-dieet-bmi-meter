import { MutationTree } from 'vuex';
import { UserState, User } from './types';

export const mutations: MutationTree<UserState> = {
    userLoaded(state, payload: User) {
        state.error = false;
        state.user = payload;
    },
    userError(state) {
        state.error = true;
        state.user = undefined;
    },
};
