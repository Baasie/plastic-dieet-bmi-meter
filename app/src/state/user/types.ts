
export interface User {
    name: string;
    nickname: string;
    picture: string;
    sub: string;
}

export interface UserState {
    user?: User;
    error: boolean;
}
