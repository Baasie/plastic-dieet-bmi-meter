import {GetterTree} from 'vuex';
import {UserState} from './types';
import {RootState} from '@/types';

export const getters: GetterTree<UserState, RootState> = {
    nickname(state) : string {
        const {user} = state;
        const nickname = (user && user.nickname) || '';
        return `${nickname}`;
    },
    imgUrl(state): string {
        const {user} = state;
        const url = (user && user.picture) || 'https://www.gravatar.com/avatar/?d=identicon';
        return `${url}`;
    },
};
