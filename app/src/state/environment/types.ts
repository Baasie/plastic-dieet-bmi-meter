
export interface Environment {
    API_URL: string;
    DOMAIN_URL: string;
}

export interface EnvironmentState {
    environment?: Environment;
    error: boolean;
}
