import { Module } from 'vuex';
import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import {EnvironmentState} from './types';
import { RootState } from '@/types';

export const state: EnvironmentState = {
    environment: undefined,
    error: false,
};

const namespaced: boolean = true;

export const environment: Module<EnvironmentState, RootState> = {
    namespaced,
    state,
    getters,
    actions,
    mutations,
};
