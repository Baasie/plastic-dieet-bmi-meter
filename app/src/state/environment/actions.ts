import {ActionTree} from 'vuex';
import axios from 'axios';
import {Environment, EnvironmentState} from './types';
import {RootState} from '../../types';


export const actions: ActionTree<EnvironmentState, RootState> = {
    fetchData({commit}): Promise<Environment> {
        return new Promise((resolve, reject) => {
            axios({
                baseURL: '/environment.json',
                timeout: 4000,
                headers: {Accept: 'application/json,application/xml'},
                method: 'GET',
            }).then((response) => {
                const payload: EnvironmentState = response && response.data;
                commit('environmentLoaded', payload);
                resolve(response.data);
            }, (error) => {
                commit('EnvironmentError');
                reject(error);
            });
        });
    },
};
