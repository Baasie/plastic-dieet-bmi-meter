import { GetterTree } from 'vuex';
import { EnvironmentState } from './types';
import { RootState } from '../../types';

export const getters: GetterTree<EnvironmentState, RootState> = {
    apiUrl(state): string {
        const { environment } = state;
        const apiUrl = (environment && environment.API_URL) || '';
        return apiUrl;
    },
    domainUrl(state): string {
        const { environment } = state;
        const domainUrl = (environment && environment.DOMAIN_URL) || '';
        return domainUrl;
    },
};
