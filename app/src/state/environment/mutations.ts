import { MutationTree } from 'vuex';
import { EnvironmentState, Environment } from './types';

export const mutations: MutationTree<EnvironmentState> = {
    environmentLoaded(state, payload: Environment) {
        state.error = false;
        state.environment = payload;
    },
    environmentError(state) {
        state.error = true;
        state.environment = undefined;
    },
};
