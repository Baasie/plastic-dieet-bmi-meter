import {BMICalculatedEvent} from '@/domain/bmi-meter-events';

export class BmiDay {
    public bmiCalculatedEvent: BMICalculatedEvent;
    public isSubmitted: boolean;
    public isBlank: boolean;

    constructor(bmiCalculatedEvent: BMICalculatedEvent, isSubmitted: boolean, isBlank: boolean) {
        this.bmiCalculatedEvent = bmiCalculatedEvent;
        this.isSubmitted = isSubmitted;
        this.isBlank = isBlank;
    }
}

