import {BmiDay} from './BmiDay';

export class BmiCardGroup {
    public cards: Array<BmiDay>;

    constructor(cards: Array<BmiDay>) {
        this.cards = cards;
    }
}
