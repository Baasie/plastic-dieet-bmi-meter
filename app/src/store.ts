import Vue from 'vue';
import Vuex, {StoreOptions} from 'vuex';
import {RootState} from '@/types';
import {environment} from '@/state/environment';
import {user} from '@/state/user';

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
    modules: {
        environment,
        user,
    },
};

export default new Vuex.Store<RootState>(store);
